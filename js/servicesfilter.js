const filterBtn = document.querySelector("#filter-btn");
const filterBtnImg = document.querySelector("#filter-btn>img");
const filterBtnLoader = document.querySelector("#filter-btn>div");
const filterBtnSpan = document.querySelector("#filter-btn>span");
const allImages = Array.from(document.querySelectorAll('.filter-container'));
let timeout;
let current = "all";
let counter = 0;
let images;
let hoverElement;

function createHover(ptext) {
    let hoverElement = document.createElement("div");
    hoverElement.classList.add('hover-container');
    hoverElement.innerHTML = `
        <div class="hover-links-container">
            <a class="hover-link first" href="#">
                <div>
                    <img src="images/svgimages/chain.svg" alt="chain">
                </div>
            </a>
            <a class="hover-link second" href="">
                <div>
                    <div></div>
                </div>
            </a>
        </div>
        <h3>CREATIVE DESIGN</h3>
        <p>${ptext}</p>`;
    return hoverElement;
}

function buttonListener() {
    filterBtn.addEventListener("click", ()=> {
        filterBtn.disabled = true;
        filterBtnLoader.classList.add("active");
        filterBtnImg.style.display = "none";
        filterBtnSpan.textContent = 'DOWNLOADING...';
        timeout = setTimeout(()=> {
            counter += 1;
            addImages(current, 2000);
            filterBtn.disabled = false;
            filterBtnLoader.classList.remove("active");
            filterBtnImg.style.display = "inline-block";
            filterBtnSpan.textContent = 'LOAD MORE';
            if (counter === 2) {
                filterBtn.disabled = true;
                filterBtn.style.visibility = "hidden"
            }
        }, 2000);
        }
    )
}

function optionsListener(){
    document.querySelector('.filter-options').addEventListener('mousedown', ev => {
        if (ev.target.id !== current) {
            current = ev.target.id;
            counter = 0;
            clearTimeout(timeout);
            filterBtn.disabled = false;
            filterBtnLoader.classList.remove("active");
            filterBtnImg.style.display = "inline-block";
            filterBtnSpan.textContent = 'LOAD MORE';
            filterBtn.style.visibility = "visible"
            document.querySelector('.filter-option.active').classList.remove('active');
            ev.target.classList.add('active');
            document.querySelectorAll('.filter-container.active').forEach((el)=>el.classList.remove('active'));
            addImages(ev.target.id);
        }
    });
}

function addImages(optionId) {
    if (counter === 2) {
        images = document.querySelectorAll('.filter-container:not(.active)');
    }
    else {
        if (optionId !== "all") {
            images = Array.from(document.querySelectorAll(`[data-filter = ${optionId}]:not(.active)`));
            if(images.length < 12 && counter !== 0) {
                images = images.concat(Array.from(document.querySelectorAll('.filter-container:not(.active)')).slice(0, 12 - images.length));
            }
        }
        else {
            images = Array.from(allImages.slice(counter*12, counter*12+12));
        }
    }
    images.forEach(el => {
        if (!el.querySelector('.hover-container')) {
            el.insertAdjacentElement("beforeend", createHover(el.dataset['hover']));
        }
        el.parentElement.insertAdjacentElement("beforeend", el);
        el.classList.add("active");
    });
}

addImages(current);
optionsListener();
buttonListener();