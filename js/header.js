function scrollListener() {
    window.addEventListener("scroll", function () {
        let header = document.querySelector(".navigation");
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        header.style.opacity = 1 - scrollTop / 450;
        if (header.style.opacity <= 0.3) {
            header.style.pointerEvents = "none";
        }
        else {
            header.style.pointerEvents = "auto";
        }
    });
}

scrollListener();