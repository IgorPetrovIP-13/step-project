const msnry = new Masonry(document.querySelector('.masonry-grid'), {
  itemSelector: '.grid-item',
  gutter: 20,
  horizontalOrder: true,
  fitWidth: true,
});

function buttonListener() {
    const masonryBtn = document.querySelector("#masonryBtn");
    const masonryBtnLoader = document.querySelector("#masonryBtn>div");
    masonryBtn.addEventListener("click", ()=> {
        masonryBtn.disabled = true;
        masonryBtnLoader.classList.add("active");
        document.querySelector("#masonryBtn>img").style.display = "none";
        document.querySelector("#masonryBtn>span").textContent = 'DOWNLOADING...';
        timeout = setTimeout(()=> {
            document.querySelectorAll('.grid-item:not(.active)').forEach(el => el.classList.add('active'));
            msnry.layout();
            masonryBtnLoader.classList.remove("active");
            masonryBtn.style.visibility = "hidden";
        }, 2000);
        }
    )
}

buttonListener();