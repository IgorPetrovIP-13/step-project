function servicesTabsListener() {
    document.querySelector('.tabs').addEventListener('mousedown', ev => {
        document.querySelector('.tabs-title.active').classList.remove('active');
        ev.target.classList.add('active');
        document.querySelector('.tab.active').classList.remove('active');
        document.querySelector(`[data-li = ${ev.target.id}]`).classList.add('active');
    });
}

servicesTabsListener();