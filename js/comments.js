let slideCounter = 1;
const allSlides = document.querySelectorAll(".slide");
const allChoose = document.querySelectorAll(".choose");

function changeSlide(n) {
    showSlides(slideCounter += n);
}

function showCurrent(n) {
    showSlides(slideCounter = n);
}

function showSlides(n) {
    if (n > allSlides.length) {
        slideCounter = 1;
    }
    else if (!n) {
        slideCounter = allSlides.length;
    }
    document.querySelector('.slide.active').classList.remove('active');
    document.querySelector('.choose.active').classList.remove('active');
    allSlides[slideCounter-1].classList.add('active');
    allChoose[slideCounter-1].classList.add('active');
}

showSlides(slideCounter);